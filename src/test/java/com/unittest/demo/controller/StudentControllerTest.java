package com.unittest.demo.controller;

import static org.hamcrest.CoreMatchers.any;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.TransactionSystemException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.unittest.demo.model.Student;
import com.unittest.demo.repository.StudentRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StudentControllerTest {
	public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
			MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private StudentRepository studentRepositoryMock;

	@InjectMocks
	private StudentController studentController;

	@Before
	public void init() {
	}

	@Test
	public void findAll_StudentsFound_ShouldReturnFoundStudentEntries() throws Exception {
		Student first = new Student(1l, "Bob", "A1234567");
		Student second = new Student(2l, "Alice", "B1234568");

		when(studentRepositoryMock.findAll()).thenReturn(Arrays.asList(first, second));

		mockMvc.perform(get("/student")).andExpect(status().isOk())
				.andExpect(content().contentType(APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$", hasSize(2)))
				.andExpect(jsonPath("$[0].id", is(1))).andExpect(jsonPath("$[0].name", is("Bob")))
				.andExpect(jsonPath("$[0].passportNumber", is("A1234567"))).andExpect(jsonPath("$[1].id", is(2)))
				.andExpect(jsonPath("$[1].name", is("Alice")))
				.andExpect(jsonPath("$[1].passportNumber", is("B1234568")));

		verify(studentRepositoryMock, times(1)).findAll();
		verifyNoMoreInteractions(studentRepositoryMock);
	}

	@Test
	public void retrieveStudent_StudentNotFound_ShouldReturnNotFoundStudent() throws Exception {
		mockMvc.perform(get("/student/100")).andExpect(status().isNotFound());
		verify(studentRepositoryMock, times(1)).findById(100L);
	}

	@Test
	public void retrieveStudent_StudentFound_ShouldReturnStudentInfomation() throws Exception {
		Student student = new Student(1l, "Bob", "A1234567");

		when(studentRepositoryMock.findById(1L)).thenReturn(Optional.of(student));

		mockMvc.perform(get("/student/1")).andExpect(status().isOk())
				.andExpect(content().contentType(APPLICATION_JSON_UTF8))
				.andExpect(jsonPath("$.name", is("Bob")))
				.andExpect(jsonPath("$.passportNumber", is("A1234567")));

		verify(studentRepositoryMock, times(1)).findById(1L);
		verifyNoMoreInteractions(studentRepositoryMock);
	}
	

	@Test
	public void createStudent_StudentWithNameIsEmpty_ShouldReturnBadRequest() throws Exception {
		Student newStudent = new Student(null, "SD2885");
		
		mockMvc.perform(post("/student")
		.contentType(MediaType.APPLICATION_JSON)
        .content(asJsonString(newStudent)))
		.andExpect(status().isBadRequest())
		.andExpect(content().contentType(APPLICATION_JSON_UTF8));
		
		verifyNoMoreInteractions(studentRepositoryMock);
	}

	@Test
	public void createStudent_StudentWithPassportNumberIsEmpty_ShouldReturnBadRequest() throws Exception {
		Student newStudent = new Student("TruongTheKiet", null);
		
		mockMvc.perform(post("/student")
		.contentType(MediaType.APPLICATION_JSON)
        .content(asJsonString(newStudent)))
		.andExpect(status().isBadRequest())
		.andExpect(content().contentType(APPLICATION_JSON_UTF8));
		
		verifyNoMoreInteractions(studentRepositoryMock);
	}
	
	@Test
	public void createStudent_StudentWithLengthPassportNumberOver15_ShouldReturnBadRequest() throws Exception {
		Student newStudent = new Student("TruongTheKiet", "A012345678912356");
		

		mockMvc.perform(post("/student")
		.contentType(MediaType.APPLICATION_JSON)
		.characterEncoding("utf8")
        .content(asJsonString(newStudent)))
		.andExpect(status().isBadRequest())
		.andExpect(content().contentType(APPLICATION_JSON_UTF8));
		
		verifyNoMoreInteractions(studentRepositoryMock);
	}

	@Test
	public void createStudent_StudentWithLengthNameOver100_ShouldReturnBadRequest() throws Exception {
		Student newStudent = new Student(
				"zasdsadasdadfsdgdfggfhdfjghjhgkghjtyyretwerfsdbdfgjyuytuteywregfdsgsdhfsshaaaaaaaaaaaasfsdfsdfsdgfdgfdgjghjhgkhgkj",
				"A012345");
		

		mockMvc.perform(post("/student")
				.contentType(MediaType.APPLICATION_JSON)
				.characterEncoding("utf8")
				.content(asJsonString(newStudent)))
				.andExpect(status().isBadRequest())
				.andExpect(content().contentType(APPLICATION_JSON_UTF8));

		verifyNoMoreInteractions(studentRepositoryMock);
	}

	@Test
	public void createStudent_StudentWithValidInformation_ShouldReturnCreated() throws Exception {
		Student newStudent = new Student("Truong The Kiet", "SD2885");
		when(studentRepositoryMock.save(Mockito.any(Student.class))).thenReturn(new Student(3L, "Truong The Kiet", "SD2885"));
		mockMvc.perform(post("/student")
				.contentType(MediaType.APPLICATION_JSON)
				.characterEncoding("utf8")
				.content(asJsonString(newStudent)))
				.andExpect(status().isCreated())
				.andExpect(content().contentType(APPLICATION_JSON_UTF8));

		verify(studentRepositoryMock, times(1)).save(Mockito.any(Student.class));
		verifyNoMoreInteractions(studentRepositoryMock);
	}
	
	@Test
	public void deleteStudent_StudentNotFound_ShouldReturnNotFound() throws Exception {
		doThrow(EmptyResultDataAccessException.class).when(studentRepositoryMock).deleteById(10L);
		mockMvc.perform(delete("/student/10"))
				.andExpect(status().isNotFound());
		verify(studentRepositoryMock, times(1)).deleteById(10L);
		verifyNoMoreInteractions(studentRepositoryMock);
	}
	
	@Test
	public void deleteStudent_StudentFound_ShouldReturnOk() throws Exception {
		doNothing().when(studentRepositoryMock).deleteById(1L);
		mockMvc.perform(delete("/student/1"))
				.andExpect(status().isOk());
		
		verify(studentRepositoryMock, times(1)).deleteById(1L);
		verifyNoMoreInteractions(studentRepositoryMock);
	}
	
	
	@Test
	public void updateStudent_StudentNotFound_ShouldReturnNotFound() throws Exception {
		Student newStudent = new Student("Bob-updated1", "E1234567");
		when(studentRepositoryMock.findById(100L)).thenReturn(Optional.empty());
		
		mockMvc.perform(put("/student/100")
		.contentType(MediaType.APPLICATION_JSON)
        .content(asJsonString(newStudent)))
		.andExpect(status().isNotFound())
		.andExpect(content().contentType(APPLICATION_JSON_UTF8));
		
		verifyNoMoreInteractions(studentRepositoryMock);
	}
	
	@Test
	public void updateStudent_StudentWithNameIsEmpty_ShouldReturnBadRequest() throws Exception {
		Student newStudent = new Student(null, "SD2885");
		
		when(studentRepositoryMock.findById(1L)).thenReturn(Optional.of(new Student(1L, "Old Name", "SD1508")));

		mockMvc.perform(put("/student/1")
		.contentType(MediaType.APPLICATION_JSON)
        .content(asJsonString(newStudent)))
		.andExpect(status().isBadRequest())
		.andExpect(content().contentType(APPLICATION_JSON_UTF8));
		
		verifyNoMoreInteractions(studentRepositoryMock);
	}

	@Test
	public void updateStudent_StudentWithPassportNumberIsEmpty_ShouldReturnBadRequest() throws Exception {
		Student newStudent = new Student("TruongTheKiet", null);
		
		when(studentRepositoryMock.findById(1L)).thenReturn(Optional.of(new Student(1L, "Old Name", "SD1508")));

		mockMvc.perform(put("/student/1")
		.contentType(MediaType.APPLICATION_JSON)
        .content(asJsonString(newStudent)))
		.andExpect(status().isBadRequest())
		.andExpect(content().contentType(APPLICATION_JSON_UTF8));
		
		verifyNoMoreInteractions(studentRepositoryMock);
	}
	
	@Test
	public void updateStudent_StudentWithLengthPassportNumberOver15_ShouldReturnBadRequest() throws Exception {
		Student newStudent = new Student("TruongTheKiet", "A012345678912356");
		
		when(studentRepositoryMock.findById(1L)).thenReturn(Optional.of(new Student(1L, "Old Name", "SD1508")));

		mockMvc.perform(put("/student/1")
		.contentType(MediaType.APPLICATION_JSON)
		.characterEncoding("utf8")
        .content(asJsonString(newStudent)))
		.andExpect(status().isBadRequest())
		.andExpect(content().contentType(APPLICATION_JSON_UTF8));
		
		verifyNoMoreInteractions(studentRepositoryMock);
	}

	@Test
	public void updateStudent_StudentWithLengthNameOver100_ShouldReturnBadRequest() throws Exception {
		Student newStudent = new Student(
				"zasdsadasdadfsdgdfggfhdfjghjhgkghjtyyretwerfsdbdfgjyuytuteywregfdsgsdhfsshaaaaaaaaaaaasfsdfsdfsdgfdgfdgjghjhgkhgkj",
				"A012345");
		
		when(studentRepositoryMock.findById(1L)).thenReturn(Optional.of(new Student(1L, "Old Name", "SD1508")));

		mockMvc.perform(put("/student/1")
				.contentType(MediaType.APPLICATION_JSON)
				.characterEncoding("utf8")
				.content(asJsonString(newStudent)))
				.andExpect(status().isBadRequest())
				.andExpect(content().contentType(APPLICATION_JSON_UTF8));

		verifyNoMoreInteractions(studentRepositoryMock);
	}

	@Test
	public void updateStudent_StudentWithValidInformation_ShouldReturnOk() throws Exception {
		Student newStudent = new Student("Truong The Kiet", "SD2885");
		when(studentRepositoryMock.findById(1L)).thenReturn(Optional.of(new Student(1L, "Old Name", "SD1508")));
		mockMvc.perform(put("/student/1")
				.contentType(MediaType.APPLICATION_JSON)
				.characterEncoding("utf8")
				.content(asJsonString(newStudent)))
				.andExpect(jsonPath("$.name", is("Truong The Kiet")))
				.andExpect(jsonPath("$.passportNumber", is("SD2885")))
				.andExpect(status().isOk())
				.andExpect(content().contentType(APPLICATION_JSON_UTF8));

		verify(studentRepositoryMock, times(1)).findById(1L);
		verify(studentRepositoryMock, times(1)).save(Mockito.any(Student.class));
		verifyNoMoreInteractions(studentRepositoryMock);
	}
	
	
	
	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
